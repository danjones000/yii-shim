<?php

namespace Danjones\YiiShim;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\CacheItem;
use Psr\Cache\CacheItemInterface;

class Cache
{
    protected $adapter;

    public function __construct()
    {
        $this->adapter = new FilesystemAdapter();
    }

    protected function getKey($key): string
    {
        $illegal = "{}()//@:";

        return str_replace(str_split($illegal), '---', $key);
    }

    public function exists($key): bool
    {
        return $this->adapter->hasItem($this->getKey($key));
    }

    public function get($key)
    {
        if (!$this->adapter->hasItem($this->getKey($key))) {
            return false;
        }

        return $this->adapter->getItem($this->getKey($key))->get();
    }

    public function getOrSet($key, callable $cb, $duration = null)
    {
        $callback = function (CacheItemInterface $item) use ($cb, $duration) {
            $item->expiresAfter($duration);

            return $cb();
        };

        return $this->adapter->get($this->getKey($key), $callback);
    }

    public function set($key, $value, $duration = null)
    {
        $item = $this->adapter->getItem($this->getKey($key));
        $item->set($value);
        $item->expiresAfter($duration);

        return $this->adapter->save($item);
    }

    public function delete($key): bool
    {
        return $this->adapter->deleteItem($this->getKey($key));
    }
}
