<?php

use Illuminate\Container\Container;
use Danjones\YiiShim\Cache;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Yii
{
    public static $app;

    protected const LOG_LEVELS = [
        0x01 => Logger::ERROR,
        0x02 => Logger::WARNING,
        0x04 => Logger::INFO,
        0x08 => Logger::DEBUG,
        0x40 => Logger::DEBUG,
    ];

    public static function log($level, $message, $category = 'yii')
    {
        if (!is_string($message)) {
            $message = json_encode($message);
        }
        $level = static::LOG_LEVELS[$level] ?? Logger::INFO;

        static::$app->log->withName($category)->addRecord($level, $message);
    }

    public static function debug($message, $category = 'application')
    {
        static::log(0x08, $message, $category);
    }

    public static function error($message, $category = 'application')
    {
        static::log(0x01, $message, $category);
    }

    public static function warning($message, $category = 'application')
    {
        static::log(0x02, $message, $category);
    }

    public static function info($message, $category = 'application')
    {
        static::log(0x04, $message, $category);
    }
}

Yii::$app = new Container();
Yii::$app->singleton('cache', function () {
    return new Cache();
});

Yii::$app->singleton('log', function () {
    $logger = new Logger('yii');
    $logger->pushHandler(new StreamHandler('log.log'), Logger::DEBUG);

    return $logger;
});
